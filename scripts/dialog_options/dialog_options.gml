// keyboard navigation
choice_length = array_length_1d(message) - 1
choice_direction = 0;
choice_direction -= max(keyboard_check_pressed(vk_up), 0)
choice_direction += max(keyboard_check_pressed(vk_down), 0)

if (dialog_options != 0) {
	choice_position += choice_direction
	
	if (choice_position < 0) {
		choice_position = choice_length
	}
	
	if (choice_position > choice_length) {
		choice_position = 0
	}
}

for(var i = 0; i < ds_list_size(json_data); i++) {
	passage = ds_list_find_value(json_data, i)
	
	if (passage[? "name"] == current_passage) {
		
		if(passage[? "choices"] != undefined ) {	
			selection = ds_list_find_value(passage[? "choices"], choice_position)
			dialog_next = selection[? "link"]
	
		} else {
			dialog_next = passage[? "link"]
		}
		
	}
}
