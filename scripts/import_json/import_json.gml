external_file = argument0

json_file = file_text_open_read(external_file);
json_data = "";

while (!file_text_eof(json_file))
{
    json_data += file_text_read_string(json_file);
    file_text_readln(json_file);
}

file_text_close(json_file);

json_map = json_decode(json_data);