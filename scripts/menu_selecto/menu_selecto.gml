// keyboard navigation
var menu_length = array_length_1d(passage[? "choices"]) - 1
var menu_direction = 0;
menu_direction -= max(keyboard_check_pressed(vk_up), 0)
menu_direction += max(keyboard_check_pressed(vk_down), 0)

if (menu_select != 0) {
	menu_position += menu_direction
	
	if (menu_position < 0) {
		menu_position = menu_length
	}
	
	if (menu_position > menu_length) {
		menu_position = 0
	}
}

// keyboard enter
var menu_clicked = max(keyboard_check_released(vk_space), keyboard_check_released(vk_enter), 0)

if (menu_clicked == 1) {
	selection = ds_list_find_value(passage[? "choices"], menu_position)
	dialog_next = selection[? "link"]
}