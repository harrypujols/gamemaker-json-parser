// settings variables
message[0] = "..."
indicator = ""
message_padding = string_height(message[0])
choice_position = 0
dialog_file = "convo.json"
dialog_data = "passages"
current_passage = "hello"
dialog_next = current_passage

// load data
import_json(dialog_file)
json_data = ds_map_find_value(json_map, dialog_data)

// process dialog initiation
for(var i = 0; i < ds_list_size(json_data); i++) {
	passage = ds_list_find_value(json_data, i)
	
	if (passage[? "name"] == current_passage) {	
		message[0] = passage[? "dialog"]
		dialog_next = passage[? "link"]
	}
}
