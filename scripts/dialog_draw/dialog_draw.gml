draw_set_halign(fa_left)
draw_set_valign(fa_middle)
draw_set_font(fnt_text)
draw_set_color(c_white)

for (i = 0; i < array_length_1d(message); i += 1) {
	draw_text(x + string_length(indicator) + message_padding, y + (i * message_padding), string(message[i]))
}

draw_text(x, y + message_padding * choice_position, string(indicator))