{
    "id": "25f9de92-4c6c-419b-99e2-fae6cbc623c2",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\/fonts\/fnt_text\/Inconsolata for Powerline.otf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata for Powerline",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "462de441-6a8e-4de9-a6d4-98a9edd91836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "83796aed-e22f-48b7-bd9b-cad03fa372b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 20,
                "y": 102
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c8a408e1-ddba-48b0-8327-47deac7948e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 229,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dd7c68d8-7a9f-4498-b69b-638dbdf56561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3a0d129c-0207-4a97-9c17-17803d1de8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "79b4b52d-f466-409d-a840-135f76090fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c1fdaaae-9656-457d-aa27-69085bb4d122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4441d1a8-4c59-43f7-b4fb-01f7ef404e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 32,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "32dd2bf4-0e42-4b43-8765-20582d965784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 183,
                "y": 77
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1c003ee9-ec59-4731-b835-afc0cfc20503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 193,
                "y": 77
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d2aa554a-7626-40f2-a091-5e74c8a3400d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "179f1f7e-c899-4492-b792-de21527b6024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 27
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7ef0045d-e9bd-432e-ac4d-7026bab4f930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ba7640ad-b32b-42b6-8046-6598dacb6154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "38e4cd8d-8d3a-4b37-b04f-46a41437f444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 26,
                "y": 102
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5ca0551e-ceb4-46df-8288-2719ba7c892a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 52
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4604bae4-00d9-43f8-a130-cfcf6c47f924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4a258649-5b90-4106-8318-4f0799eb2ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 238,
                "y": 77
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dfd8b0fb-41ae-4eef-921c-bd0a924eaf2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e33ac637-0019-4983-bd64-2de76f839370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "32340b61-894e-428a-8bad-e81f60c8a814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 204,
                "y": 27
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a9ace770-3da1-443b-8df6-73804e778a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "22d1d212-0234-40af-8421-766d7d27ea18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d25ce448-d60e-4c59-862a-7d79ab5b0da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 52
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7806b5ce-2313-47f1-b76d-e8e6bab32131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 79,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ecb544e9-baf4-453a-a831-51ac537cd088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f3d5a193-d5eb-4001-88b6-009c709208d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 14,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "09f0ab40-28d0-4c1d-bf79-65a647c511ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 8,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ad399216-9342-4b38-ac7b-db778f254dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 27
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5d7afaf7-7d2e-4f7d-b524-5c64dadca323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 27
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "073f471d-a369-4cec-9ec4-ca78adc65993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 27
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f40d906e-c28c-4681-8dbe-c48ccdde88ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 52
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "43b9f735-286c-4da8-a5dc-ed38595c3a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 27
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ef21847b-c1f1-4817-9f7d-c026bacff8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b262949a-6e54-4150-84c3-8f407b21d213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 200,
                "y": 52
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a7e10684-bd67-47fc-908b-77e78ca19064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bb6e9142-c23b-42d0-939f-dd3fea214b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 52
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "14ba330f-cf3c-42e9-84dc-3c1fad0b9bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 222,
                "y": 52
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3188b059-0a85-472f-99b9-5da5af328389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 52
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a7e3e222-586a-47a0-bde0-b34f1a4aa983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9d2783f9-4a8d-47de-9aed-ea25c5211d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 52
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2a618697-fe18-43e4-9547-9b059f11d79c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 143,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "08dee02c-fb28-4ce1-ae50-adcf2f16d0ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d4c16fe8-c661-4dab-9eaf-524827d398a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4628b685-a9d4-4320-a412-535877df4c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0cc9f0c0-4e32-436f-bca4-c35eb57f3ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "eba3745b-68d0-4e52-a036-3f517ec4b66e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 77
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "58a36222-8fd2-4815-9a99-a48878815506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "01e522f0-ed98-42da-a3e1-0a2516bb835b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 77
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3c0cecc6-0483-4d32-8235-fb7b51ffa362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "14519414-4b70-4f38-8c27-165096164b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 77
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6815164e-02ab-46c9-bcfb-2883538e3829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b50f8e18-796d-4b52-9d9c-7e2d0c8d16fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 27
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5ba14ee7-9b9d-4b27-8d66-4cc3385af4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 77
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "93ce2053-7b49-4e91-8458-c2819d5f2060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d07b95d6-b11c-4b10-9dcc-2244fddb0b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "164c3602-6fa7-4770-b3be-50a955f3ee76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ffbb21f2-99a9-4932-a419-ce03a7c62591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f382cc7-a3de-418c-b2a5-600183468c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 182,
                "y": 27
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "070ca33f-8c49-49ac-81e7-1051113b1586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 220,
                "y": 77
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0f966aaa-da15-4a4b-972b-a1c54fad0781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 193,
                "y": 27
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7578de3c-c12c-4b30-8d24-124d14f44db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 211,
                "y": 77
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cbcd3bb6-ef08-4840-bc65-e83291f93ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 153,
                "y": 77
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0eea8b22-c0c6-4841-af9d-af1c216e4f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 27
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c1ca5d74-ed15-48b2-a20a-73e0be63bd76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 246,
                "y": 77
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b4f17fd4-ccbf-4017-add7-bd1dfd16fa60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 215,
                "y": 27
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9dd26ef5-42c6-479a-bcf3-3c5d4898a357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 226,
                "y": 27
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cb769a2a-f418-4e8c-a44d-f3b72153a866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 237,
                "y": 27
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "510dbe40-b397-48b1-8767-2709672d3210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 27
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "833e0ab6-b5ae-4a2d-b420-c5f42610a3c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 52
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bc1246a0-361c-4edf-ad2a-29fad33cddd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 27
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f2eea2f2-5c62-42f7-8401-38a896748890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ba564e98-973a-4794-bfe1-36be57694f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7caacdf5-4268-4462-8bfa-5a8e88f2860b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 202,
                "y": 77
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "877082a2-d939-456e-895b-65a2a9510b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 173,
                "y": 77
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8e980e71-9bfb-4975-94af-f966d86547c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 27
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b75319a0-21aa-4dd3-ab60-c8fd5f31943e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 163,
                "y": 77
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6547f6c1-734e-4b29-bc1b-174f25409a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "13730065-bc23-461f-9a69-691080a102c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aadadc92-2158-46f0-99d4-1dfdccecfbb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5c04de18-9af9-4c62-91fa-14557fa1819a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 79,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c0959dca-f1ef-4c98-9503-cfae9c21e57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "588ee9aa-e44d-4db5-8551-62474a033371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 133,
                "y": 77
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "64a25101-5262-4ea0-bac5-da36af6cc1ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ecf48efd-6fee-4469-95a5-2629a4717085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 52
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6a39e24d-097f-4e2e-b093-579b5f03a58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2965f283-557e-40c1-b803-a5857bdb16e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d45dc284-75ed-4fd2-9477-68a956dfd80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "95d01168-e8e2-4fc0-96ed-3ba235f2f038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 52
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1a039a8f-8a71-4c1c-910e-9089659f2494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8f6e4be1-d5f1-4b7b-9889-823ed465e929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 52
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a96bdf17-c988-4e2c-9638-e30496976f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 123,
                "y": 77
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6b70aa6-3564-4254-8e9e-7c05403fe569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 37,
                "y": 102
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a88654aa-780c-42ec-bd0e-176afd8a2763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 77
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c0e3ccfe-95ce-4cf8-8c71-dc54d302d79c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 145,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}