for(var i = 0; i < ds_list_size(json_data); i++) {
	passage = ds_list_find_value(json_data, i)
	
	if (passage[? "name"] == current_passage) {
		
		if(passage[? "choices"] != undefined ) {	
			indicator = ">"
			
			for (var m = 0; m < ds_list_size(passage[? "choices"]); m++) {			
				choice = ds_list_find_value(passage[? "choices"], m)
				message[m] = choice[? "dialog"]
			}
				
		} else {
			indicator = ""
			message = 0
			message[0] = passage[? "dialog"]
			//dialog_next = passage[? "link"]
		}
		
	}
}

