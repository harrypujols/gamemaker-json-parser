{
    "id": "632a745e-f929-45bc-b900-bea2e43cbe3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text",
    "eventList": [
        {
            "id": "5b12d3b4-2566-4bb6-a753-168ea79faf0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "632a745e-f929-45bc-b900-bea2e43cbe3d"
        },
        {
            "id": "abc751d2-a15d-49c2-89ca-ebe5c3880e3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "632a745e-f929-45bc-b900-bea2e43cbe3d"
        },
        {
            "id": "c5a7aa51-6385-4f95-a725-93dbf204d19c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "632a745e-f929-45bc-b900-bea2e43cbe3d"
        },
        {
            "id": "ed0fc39e-2a34-4367-9da1-ed262bc8a96e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "632a745e-f929-45bc-b900-bea2e43cbe3d"
        },
        {
            "id": "dd423df5-3ee9-45c3-9b56-8022e9b13ea1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "632a745e-f929-45bc-b900-bea2e43cbe3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33fdb439-a320-46a3-87f1-125e3a6ce714",
    "visible": true
}